Name:       cern-anaconda-addon
Version:    1.12
Release:    1%{?dist}
Summary:    CERN configuration anaconda addon

Group:      CERN/Utilities
License:    GPLv2
URL:        http://linux.cern.ch
Source0:    %{name}-%{version}.tgz

BuildArch: noarch
Requires: locmap-firstboot
Requires: initial-setup-gui

%description
CERN configuration anaconda addon.

%prep
%setup -q

%install

install -d %{buildroot}/%{_datadir}/anaconda/addons/
install -d %{buildroot}/%{_datadir}/icons/hicolor/scalable/apps/
install -d %{buildroot}/%{_datadir}/anaconda/dbus/confs/
install -d %{buildroot}/%{_datadir}/anaconda/dbus/services/
cp -ra cern_customizations %{buildroot}/%{_datadir}/anaconda/addons/
install -p -m 644 data/cern.svg %{buildroot}/%{_datadir}/icons/hicolor/scalable/apps/
install -p -m 644 data/org.fedoraproject.Anaconda.Addons.CernCustomizations.conf %{buildroot}/%{_datadir}/anaconda/dbus/confs/
install -p -m 644 data/org.fedoraproject.Anaconda.Addons.CernCustomizations.service %{buildroot}/%{_datadir}/anaconda/dbus/services/

%files
%{_datadir}/anaconda/addons/cern_customizations
%{_datadir}/icons/hicolor/scalable/apps/cern.svg
%{_datadir}/anaconda/dbus/confs/org.fedoraproject.Anaconda.Addons.CernCustomizations.conf
%{_datadir}/anaconda/dbus/services/org.fedoraproject.Anaconda.Addons.CernCustomizations.service

%doc LICENSE README.md

%changelog
* Fri Oct 11 2024 Ben Morrice <ben.morrice@cern.ch> - 1.12-1
- Allow user to choose if locmap auto reconfiguration should be enabled
- remove python2 remnants

* Mon Oct 09 2023 Ben Morrice <ben.morrice@cern.ch> - 1.11-1
- Allow user to choose /afs or /home for home directories
- Add puppet-resolved as a default puppet module on 8+

* Wed Jan 25 2023 Ben Morrice <ben.morrice@cern.ch> - 1.10-1
- Fix release detection, effectively adding AlmaLinux/RHEL support

* Wed Jul 13 2022 Ben Morrice <ben.morrice@cern.ch> - 1.9-2
- Add explict requires for initial-setup-gui

* Wed Jun 15 2022 Ben Morrice <ben.morrice@cern.ch> - 1.9-1
- Add a D-Bus service to perform the work for the addon

* Tue Mar 29 2022 Ben Morrice <ben.morrice@cern.ch> - 1.8-5
- Add nscd as a default module for the 8 family

* Tue Sep 07 2021 Ben Morrice <ben.morrice@cern.ch> - 1.8-4
- Add postfix as a default module for the 8 family

* Tue May 11 2021 Ben Morrice <ben.morrice@cern.ch> - 1.8-3
- Add user feedback when applying changes

* Tue Apr 13 2021 Ben Morrice <ben.morrice@cern.ch> - 1.8-2
- Simplify deployment of dnf-autoupdate on 8 family systems

* Thu Mar 18 2021 Ben Morrice <ben.morrice@cern.ch> - 1.8-1
- Add support for CentOS Stream 8
- Use dnf-automatic instead of yum-autoupdate on 8 family systems

* Tue Jan 28 2020 Ben Morrice <ben.morrice@cern.ch> - 1.7-1
- Add support for C8

* Tue Nov 27 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.6-1
- Disable EOSFUSE choice at install time
- Promote CVMFS out of Beta

* Mon May 07 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.5-1
- Disable gnome-initial-setup globally

* Mon May 07 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.4-1
- Rebuild for 7.5

* Mon Sep 11 2017 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.3-1
- Delegate locmap execution to systemd

* Wed Sep 06 2017 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.0-1
- First version for CentOS 7.4
