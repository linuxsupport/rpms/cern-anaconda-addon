### Anaconda development

As we support multiple distributions a tool `adev.sh` has been written to facilitate rapid testing, utilising podman.

initial-setup is graphical and requires a functional systemd installation, thus running through podman requires a bit of juggling. To achieve this, podman runs as root and the containers spawned are `--privileged`.

Images created by the script have an entry point of `/sbin/init` and have both `rsyslog` and `sshd` running as enabled services. The script communicates to the pod via ssh with X forwarding for the graphical component. `rsyslog` ensures that `/var/log/messages` is populated which can be useful whilst debugging.

```
$ ./adev.sh
usage: adev.sh <alma8|alma9> <deploy|shell|updatebase|kill>
```


The script is most likely invoked with the `deploy` option which will:
* build a 'base' image should one not exist (including dependencies for `initial-setup-graphical`)
* build the rpm for the release requested
* build a 'cern' image, from the 'base' image including the built rpm
* run the 'cern' image
* wait until ssh is up, and then launch `/usr/libexec/initial-setup/initial-setup-graphical`

The `shell` option will launch a ssh connection to the image requested

The `updatebase` option will generate a new 'base' image with `yum clean all && yum -y update`

`kill` will stop the running pod
