#!/bin/bash


function usage {
  echo "usage: $0 <alma8|alma9> <deploy|shell|updatebase|kill>"
  exit
}

function preflight_check {
  for package in "rpm-build nmap-ncat podman"
  do
    rpm -q $package &>/dev/null
    if [ $? -ne 0 ]; then
      echo "$package does not appear to be installed ..."
      exit
    fi
  done
  timeout 2 sudo -n podman --help &>/dev/null
  if [ $? -ne 0 ]; then
    echo "sudo access to podman is required, which doesn't appear to the case..."
    exit
  fi
}

function is_running {
  IS_RUNNING=`sudo podman ps |grep $1 | wc -l`
  return $IS_RUNNING
}

function kill_pod {
  is_running $1
  if [ $? -eq 1 ]; then
    echo "pod $1 is currently running, killing pod"
    sudo podman ps | grep $1 | awk '{print $1}' | xargs sudo podman kill
  fi
}

function run_pod {
  kill_pod $1
  echo "booting $1 pod"
  sudo podman run -p $SSHPORT:22 -d --privileged --network=bridge $1
}

function run_ssh {
  for i in `seq 1 30`; do
    TEST_CONNECTION=$( nc -i 0.1 --recv-only localhost $SSHPORT 2>/dev/null |grep -i openssh |wc -l )
    if [ $TEST_CONNECTION -eq 1 ]; then
      break
    fi
    sleep 0.3
  done
  if [ $TEST_CONNECTION -eq 1 ]; then
    ssh root@localhost -p$SSHPORT -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no  -i $HOME/$PODMAN_TAG.ssh -Y $1
  else
    echo "unable to ssh to pod, you probably need to look at something :?"
    exit
  fi
}

function build_base {
  HAVE_BASE=`sudo podman image list |grep $1 | wc -l`
  if [ $HAVE_BASE -eq 0 ]; then
    echo "Base image missing, building now"
    PODMAN_BUILD_DIR=`mktemp -d`
    cd $PODMAN_BUILD_DIR
    if [ ! -f $HOME/$PODMAN_TAG.ssh ] || [ ! -f $HOME/$PODMAN_TAG.ssh.pub ]; then
      ssh-keygen -f $PODMAN_TAG.ssh -P ""
      cp $PODMAN_TAG.ssh $HOME
      cp $PODMAN_TAG.ssh.pub $HOME
    else
      cp $HOME/$PODMAN_TAG.ssh.pub .
    fi

    cat >$PODMAN_BUILD_DIR/Dockerfile <<EOF
FROM gitlab-registry.cern.ch/linuxsupport/$OSRELEASE-base

RUN yum -y install openssh-server openssh-clients xauth rsyslog
RUN yum -y install initial-setup-gui
COPY $PODMAN_TAG.ssh.pub /root/.ssh/authorized_keys
RUN chmod 0644 /root/.ssh/authorized_keys

CMD [ "/sbin/init" ]
EOF
    sudo podman build --pull -t ${PODMAN_TAG}_${OSRELEASE}_base .
  fi

}

function update_base {
  build_base $1
  PODMAN_BUILD_DIR=`mktemp -d`
   cat >$PODMAN_BUILD_DIR/Dockerfile <<EOF
FROM $1
RUN yum clean all
RUN yum -y update
CMD [ "/sbin/init" ]
EOF
  cd $PODMAN_BUILD_DIR
  sudo podman build -t $1 .

}

preflight_check

PODMAN_TAG=anaconda_dev
GIT_ROOT=`git rev-parse --show-toplevel 2>/dev/null`
if [ $? -ne 0 ]; then
  echo "unable to determine cern-anaconda-addon git directory, please clone from git"
  exit
fi

if [ $# -ne 2 ]; then
  usage
fi
if [ "$1" == "alma8" ]; then
  OSRELEASE=alma8
  RPMDIST=.al8.cern
  SSHPORT=22222
elif [ "$1" == "alma9" ]; then
  OSRELEASE=alma9
  RPMDIST=.al9.cern
  SSHPORT=32222
else
  echo "$1 is not supported"
  usage
fi
if [ "$2" == "deploy" ]; then
  build_base ${PODMAN_TAG}_${OSRELEASE}_base
  cd $GIT_ROOT
  export DIST=$RPMDIST
  make rpm 
  RPM=`ls -1r $GIT_ROOT/build/RPMS/noarch/*$RPMDIST* | head -1`
  PODMAN_BUILD_DIR=`mktemp -d`
  cp $RPM $PODMAN_BUILD_DIR
  cd $PODMAN_BUILD_DIR
  cat >$PODMAN_BUILD_DIR/Dockerfile <<EOF
FROM ${PODMAN_TAG}_${OSRELEASE}_base

COPY *rpm /root/
RUN dnf --repo=cern -y install /root/*rpm
RUN touch /root/anaconda-ks.cfg

CMD [ "/sbin/init" ]
EOF
  sudo podman build -t ${PODMAN_TAG}_${OSRELEASE}_cern .

  run_pod ${PODMAN_TAG}_${OSRELEASE}_cern
  run_ssh /usr/libexec/initial-setup/initial-setup-graphical
elif [ "$2" == "shell" ]; then
  is_running ${PODMAN_TAG}_${OSRELEASE}_cern
  if [ $? -eq 0 ]; then
    echo "pod ${PODMAN_TAG}_${OSRELEASE}_cern is not running, let me fix that for you"
    run_pod ${PODMAN_TAG}_${OSRELEASE}_cern
  fi
  run_ssh
elif [ "$2" == "kill" ]; then
  kill_pod ${PODMAN_TAG}_${OSRELEASE}_cern
elif [ "$2" == "updatebase" ]; then
  update_base ${PODMAN_TAG}_${OSRELEASE}_base
else
  echo "$2 not supported"
  usage
fi
