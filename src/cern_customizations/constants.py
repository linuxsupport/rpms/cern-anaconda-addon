import re
release_string=open('/etc/redhat-release','r').read().rstrip()
release = re.search('\d+',release_string).group()

if release == '8' or release == '9':
    LOCMAP_CORE_MODULES = ["ssh", "sudo", "chrony", "kerberos", "lpadmin", "postfix", "nscd", "resolved"]
# unsupported future distribution
else:
    LOCMAP_CORE_MODULES = []

LOCMAP_BIN = "/usr/bin/locmap"
LOCMAP_SYSTEMD_UNIT = "locmap-firstboot-graphical.service"
SYSTEMCTL_BIN = "/usr/bin/systemctl"

SYSCONFIG_FILE_PATH = "/etc/sysconfig/locmap-initialsetup"
SYSCONFIG_YUM_PATH = "/etc/sysconfig/yum-autoupdate"
GDMCONFIG_FILE_PATH = "/etc/gdm/custom.conf"
XDGCONFIG_FILE_PATH = "/etc/xdg/autostart/gnome-initial-setup-first-login.desktop"

ADDON_LOG_FILE = "/root/addon_cern_customizations.log"

RUN_IS_DEFAULT = True
AFS_IS_DEFAULT = True
CVMFS_IS_DEFAULT = False
GNOME_INIT_DEFAULT = False
EOS_IS_DEFAULT = False
AUTOUPDATE_IS_DEFAULT = True
AUTOUPDATEBOOT_IS_DEFAULT = False


from dasbus.identifier import DBusServiceIdentifier
from pyanaconda.core.dbus import DBus
from pyanaconda.modules.common.constants.namespaces import ADDONS_NAMESPACE

# These define location of the addon's service on D-Bus. See also the data/*.conf file.

CERN_NAMESPACE = (*ADDONS_NAMESPACE, "CernCustomizations")

CERN = DBusServiceIdentifier(
    namespace=CERN_NAMESPACE,
    message_bus=DBus
)
