import logging
import os
import sys
import subprocess
import re

from configparser import SafeConfigParser, NoOptionError, NoSectionError
from pyanaconda.core import util
from pyanaconda.modules.common.task import Task

from cern_customizations.constants import SYSCONFIG_FILE_PATH, \
            AFS_IS_DEFAULT, EOS_IS_DEFAULT, CVMFS_IS_DEFAULT, \
            AUTOUPDATE_IS_DEFAULT, RUN_IS_DEFAULT, LOCMAP_BIN, \
            SYSTEMCTL_BIN, SYSCONFIG_YUM_PATH, \
            LOCMAP_CORE_MODULES, AUTOUPDATEBOOT_IS_DEFAULT, \
            GNOME_INIT_DEFAULT, GDMCONFIG_FILE_PATH, \
            LOCMAP_SYSTEMD_UNIT, XDGCONFIG_FILE_PATH


# C8
try:
  from pyanaconda.core.configuration.anaconda import conf
  system_root = conf.target.system_root
# older versions
except:
  from pyanaconda.core.util import getSysroot
  system_root = getSysroot()

log = logging.getLogger(__name__)

__all__ = [ "CernCustomizationsInstallationTask"]

class CernCustomizationsInstallationTask(Task):

    def __init__(self, run, afs, homedirectory, autoupdate, cvmfs, gnomeinit, locmapreconfigure):
        """Create a task."""
        super().__init__()
        self._run = run
        self._afs = afs
        self._homedirectory = homedirectory
        self._autoupdate = autoupdate
        self._cvmfs = cvmfs
        self._locmapreconfigure = locmapreconfigure
        self._gnomeinit = gnomeinit

    @property
    def name(self):
        return "Run CERN customizations"

    def run(self):
        """Run the task."""

        yumupdate_file = os.path.normpath(system_root + SYSCONFIG_YUM_PATH)
        # Data is persisted in /root/initial-setup-ks.cfg
        sysconfig_file = os.path.normpath(system_root + SYSCONFIG_FILE_PATH)
        with open(sysconfig_file, "w") as fobj:
            fobj.write("LOCMAP_FIRSTBOOT_START=%s\n" % self._run)
            # For compatibility but can be dropped in >= 7.5
            fobj.write("LOCMAP_FIRSTBOOT_STARTAFS=%s\n" % self._afs)
            #fobj.write("LOCMAP_FIRSTBOOT_EOSCLIENT=%s\n" % self.eos)
            fobj.write("LOCMAP_FIRSTBOOT_CVMFS=%s\n" % self._cvmfs)
            fobj.write("LOCMAP_FIRSTBOOT_UPDATE=%s\n" % self._autoupdate)
            fobj.write("LOCMAP_HOMEDIRECTORY_LOCAL=%s\n" % self._homedirectory)
            fobj.close()
        # Allow to disable gnome-initial-setup
        gdmconfig_file = os.path.normpath(system_root + GDMCONFIG_FILE_PATH)
        xdgconfig_file = os.path.normpath(system_root + XDGCONFIG_FILE_PATH)
        if True:
        #FIX FIX FIX FIX if not self._gnomeinit:
            config = SafeConfigParser()
            config.optionxform = str
            config.read(gdmconfig_file)
            try:
                config.set('daemon', 'InitialSetupEnable', 'False')
                with open(gdmconfig_file, "w") as fobj:
                    config.write(fobj)
                    fobj.close()
            except (NoOptionError,NoSectionError):
                print("Unexpected error:", sys.exc_info()[0])

            configxdg = SafeConfigParser()
            configxdg.optionxform = str
            configxdg.read(xdgconfig_file)
            try:
                configxdg.set('Desktop Entry', 'X-GNOME-Autostart-enabled', 'false')
                with open(xdgconfig_file, "w") as fobj:
                    configxdg.write(fobj)
                    fobj.close()
            except (NoOptionError,NoSectionError):
                print("Unexpected error:", sys.exc_info()[0])

        # we will need this for later
        release_string=open('/etc/redhat-release','r').read().rstrip()
        release = re.search('\d+',release_string).group()

        # Configure yum-autoupdate
        if self._autoupdate:
            subprocess.call(["dnf", "-y", "install", "yum-autoupdate"])
        else:
            # we use call rather than check_call as we don't care if this fails
            # due to yumautoupdate not being installed
            subprocess.call([SYSTEMCTL_BIN, "disable", "dnf-automatic-install.timer"])

        # locmap may not be installed - force install if it's not
        locmap_installed=os.path.isfile('/usr/bin/locmap')
        if locmap_installed == False:
            subprocess.call(["dnf", "-y", "install", "locmap-release"])
            subprocess.call(["dnf", "-y", "install", "locmap"])

        # the above call to install locmap could have failed if the host is
        # outside of CERN...
        locmap_installed=os.path.isfile('/usr/bin/locmap')
        if locmap_installed == True:
            # Enable locmap core modules by default
            for module in LOCMAP_CORE_MODULES:
                subprocess.call([LOCMAP_BIN, "--enable", module])

          # Configure and run locmap only if not disabled
            if self.run:
                subprocess.call([SYSTEMCTL_BIN, "enable", LOCMAP_SYSTEMD_UNIT])
                try:
                    if self._afs:
                        subprocess.check_call([LOCMAP_BIN, "--enable", "afs"])
                    else:
                        subprocess.check_call([LOCMAP_BIN, "--disable", "afs"])
                except subprocess.CalledProcessError:
                    print("Couldn't run locmap successfully")
                except:
                    print("Unexpected error:", sys.exc_info()[0])
                try:
                    if self._cvmfs:
                        subprocess.check_call([LOCMAP_BIN, "--enable", "cvmfs"])
                    else:
                        subprocess.check_call([LOCMAP_BIN, "--disable", "cvmfs"])
                except subprocess.CalledProcessError:
                    print("Couldn't run locmap successfully")
                except:
                    print("Unexpected error:", sys.exc_info()[0])
                try:
                    if self._locmapreconfigure:
                        subprocess.check_call([LOCMAP_BIN, "--enable_autoreconfigure"])
                except subprocess.CalledProcessError:
                    print("Couldn't run locmap successfully")
                except:
                    print("Unexpected error:", sys.exc_info()[0])
