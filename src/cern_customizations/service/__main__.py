from pyanaconda.modules.common import init
init()  # must be called before importing the service code

from cern_customizations.service.cern import CernCustomizations
service = CernCustomizations()
service.run()
