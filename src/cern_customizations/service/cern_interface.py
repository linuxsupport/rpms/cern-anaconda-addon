from dasbus.server.interface import dbus_interface
from dasbus.server.property import emits_properties_changed
from dasbus.typing import *  # pylint: disable=wildcard-import,unused-wildcard-import

from pyanaconda.modules.common.base import KickstartModuleInterface

from cern_customizations.constants import CERN

import logging

log = logging.getLogger(__name__)


@dbus_interface(CERN.interface_name)
class CernCustomizationsInterface(KickstartModuleInterface):

    def connect_signals(self):
        super().connect_signals()

    @property
    def Configured(self) -> Bool:
        return self.implementation.Configured

    @emits_properties_changed
    def SetConfigured(self, configured: Bool):
        self.implementation.set_configured(configured)

    @property
    def Afs(self) -> Bool:
        return self.implementation.Afs

    @emits_properties_changed
    def SetAfs(self, afs: Bool):
        self.implementation.set_afs(afs)

    @property
    def Cvmfs(self) -> Bool:
        return self.implementation.Cvmfs

    @emits_properties_changed
    def SetCvmfs(self, cvmfs: Bool):
        self.implementation.set_cvmfs(cvmfs)

    @property
    def Run(self) -> Bool:
        return self.implementation.Run

    @emits_properties_changed
    def SetRun(self, run: Bool):
        self.implementation.set_run(run)

    @property
    def AutoUpdate(self) -> Bool:
        return self.implementation.AutoUpdate

    @emits_properties_changed
    def SetAutoUpdate(self, autoupdate: Bool):
        self.implementation.set_autoupdate(autoupdate)

    @emits_properties_changed
    def SetHomeDirectory(self, homedirectory: Bool):
        self.implementation.set_homedirectory(homedirectory)

    @emits_properties_changed
    def SetLocmapReconfigure(self, locmapreconfigure: Bool):
        self.implementation.set_locmapreconfigure(locmapreconfigure)

    @property
    def GnomeInit(self) -> Bool:
        return self.implementation.GnomeInit

    @emits_properties_changed
    def SetGnomeInit(self, gnomeinit: Bool):
        self.implementation.set_gnomeinit(gnomeinit)

