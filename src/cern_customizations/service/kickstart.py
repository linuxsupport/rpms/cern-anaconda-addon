
"""Module with the CernData class."""

import os.path
import subprocess
import sys

import logging

from pyanaconda.core.kickstart import VERSION, KickstartSpecification
from pyanaconda.core.kickstart.addon import AddonData

from inspect import signature, Parameter

try:
  from pyanaconda.core.configuration.anaconda import conf
  system_root = conf.target.system_root
# older versions
except:
  from pyanaconda.core.util import getSysroot
  system_root = getSysroot()

from cern_customizations.constants import SYSCONFIG_FILE_PATH, \
            AFS_IS_DEFAULT, EOS_IS_DEFAULT, CVMFS_IS_DEFAULT, \
            AUTOUPDATE_IS_DEFAULT, RUN_IS_DEFAULT, LOCMAP_BIN, \
            SYSTEMCTL_BIN, SYSCONFIG_YUM_PATH, \
            LOCMAP_CORE_MODULES, AUTOUPDATEBOOT_IS_DEFAULT, \
            GNOME_INIT_DEFAULT, GDMCONFIG_FILE_PATH, \
            LOCMAP_SYSTEMD_UNIT, XDGCONFIG_FILE_PATH

from pykickstart.options import KSOptionParser
from pykickstart.errors import KickstartParseError, formatErrorMsg

from pykickstart.version import versionToLongString, RHEL8

log = logging.getLogger(__name__)

class CernCustomizationsData(AddonData):
    """
    Class parsing and storing data for the CERN addon.

    :see: pyanaconda.addons.AddonData
    """
    def __init__(self):
        """
        Initialisation

        :param name: name of the addon
        :type name: str
        """
        super().__init__()
        self.run = RUN_IS_DEFAULT
        self.afs = AFS_IS_DEFAULT
        self.autoupdate = AUTOUPDATE_IS_DEFAULT
        self.autoupdateboot = AUTOUPDATEBOOT_IS_DEFAULT
        #self.eos = EOS_IS_DEFAULT
        self.cvmfs = CVMFS_IS_DEFAULT
        self.gnomeinit = GNOME_INIT_DEFAULT
        self.configured = False

    def __str__(self):
        """
        What should end up in the resulting kickstart file.
        """
        addon_str = "%%addon cern_customizations "
        if self.run:
            addon_str += " --run-locmap"
        if self.afs:
            addon_str += " --afs-client"
        if self.cvmfs:
            addon_str += " --cvmfs-client"
        #if self.eos:
        #    addon_str += " --eos-client"
        if self.autoupdate:
            addon_str += " --auto-update"
        if self.autoupdateboot:
            addon_str += " --auto-update-boot"
        if self.gnomeinit:
            addon_str += " --gnome-initial-setup"
        addon_str += "\n%end\n"
        return addon_str

    def handle_header(self, lineno, args):
        """
        The handle_header method is called to parse additional arguments in the
        %addon section line.
        args is a list of all the arguments following the addon ID. For
        example, for the line:
            %addon org_fedora_hello_world --reverse --arg2="example"
        handle_header will be called with args=['--reverse', '--arg2="example"']

        :param lineno: the current line number in the kickstart file
        :type lineno: int
        :param args: the list of arguments from the %addon line
        :type args: list
        """

        opc = KSOptionParser(version=RHEL8, description='CERN addon',prog='CERN')
        opc.add_argument("--afs-client", action="store_true", default=True,
                       dest="afs", help="Install and configure Openafs client",
                       version=RHEL8)

        opc.add_argument("--auto-update", action="store_true", default=True,
                       dest="autoupdate", help="Enable automatic system updates",
                       version=RHEL8)

        opc.add_argument("--auto-update-boot", action="store_true", default=False,
                       dest="autoupdateboot", help="Enable automatic system updates at boot",
                       version=RHEL8)

        opc.add_argument("--cvmfs-client", action="store_true", default=False,
                       dest="cvmfs", help="Install and configure Fuse CVMFS client",
                       version=RHEL8)

        opc.add_argument("--gnome-initial-setup", action="store_true", default=False,
                       dest="gnomeinit", help="Run Gnome initial setup after first login",
                       version=RHEL8)

        opc.add_argument("--run-locmap", action="store_true", default=True,
                       dest="run", help="Run locmap at firstboot",
                       version=RHEL8)
        opts = opc.parse_args(args=args, lineno=lineno)

        self.afs = opts.afs
        self.run = opts.run
        self.cvmfs = opts.cvmfs
        self.autoupdate = opts.autoupdate
        self.autoupdateboot = opts.autoupdateboot
        self.gnomeinit = opts.gnomeinit
        self.configured = True

    def handle_line(self, line):
        """
        The handle_line method that is called with every line from this addon's
        %addon section of the kickstart file.

        :param line: a single line from the %addon section
        :type line: str
        """
        pass

    def finalize(self):
        """
        The finalize method that is called when the end of the %addon section
        (i.e. the %end line) is processed. An addon should check if it has all
        required data. If not, it may handle the case quietly or it may raise
        the KickstartValueError exception.
        """
        pass

    def setup(AddonDataSetupArgs):
        """
        The setup method that should make changes to the runtime environment
        according to the data stored in this object.

        :param storage: object storing storage-related information
        (disks, partitioning, bootloader, etc.)
        :type storage: blivet.Blivet instance
        :param ksdata: data parsed from the kickstart file and set in the
        installation process
        :type ksdata: pykickstart.base.BaseHandler instance
        :param instclass: distribution-specific information
        :type instclass: pyanaconda.installclass.BaseInstallClass
        :param payload: object managing packages and environment groups
        for the installation
        :type payload: any class inherited from the pyanaconda.packaging.Payload
        class
        """
        pass

    @property
    def mandatory(self):
        """
        The mandatory property that tells whether the spoke is mandatory to be
        completed to continue in the installation process.

        :rtype: bool

        """

        # this is a mandatory spoke, user must check it
        return True

class CernCustomizationsKickstartSpecification(KickstartSpecification):

    version = VERSION

    addons = {
        "cern_customizations": CernCustomizationsData
    }
