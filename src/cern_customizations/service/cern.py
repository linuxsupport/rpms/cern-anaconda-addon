from pyanaconda.core.configuration.anaconda import conf
from pyanaconda.core.dbus import DBus
from pyanaconda.core.signal import Signal
from pyanaconda.modules.common.base import KickstartService
from pyanaconda.modules.common.containers import TaskContainer

from cern_customizations.constants import CERN
from cern_customizations.service.cern_interface import CernCustomizationsInterface
from cern_customizations.service.kickstart import CernCustomizationsKickstartSpecification
from cern_customizations.service.installation import CernCustomizationsInstallationTask


import logging

log = logging.getLogger(__name__)


class CernCustomizations(KickstartService):

    def __init__(self):
        super().__init__()

        self._configured = False
        self._afs = True
        self._homedirectory = True
        self._cvmfs = False
        self._run = False
        self._autoupdate = True
        self._gnomeinit = True
        self._locmapreconfigure = False

        self.configured_changed = Signal()
        self.afs_changed = Signal()
        self.cvmfs_changed = Signal()
        self.run_changed = Signal()
        self.autoupdate_changed = Signal()
        self.homedirectory_changed = Signal()
        self.gnomeinit_changed = Signal()
        self.locmapreconfigure_changed = Signal()

    def publish(self):
        """Publish the module."""
        TaskContainer.set_namespace(CERN.namespace)
        DBus.publish_object(CERN.object_path, CernCustomizationsInterface(self))
        DBus.register_service(CERN.service_name)

    @property
    def kickstart_specification(self):
        """Return the kickstart specification."""
        return CernCustomizationsKickstartSpecification

    def process_kickstart(self, data):
        """Process the kickstart data."""
        log.debug("Processing kickstart data...")

    def setup_kickstart(self, data):
        """Set the given kickstart data."""
        log.debug("Generating kickstart data...")

    # to change
    @property
    def Configured(self):
        return self._configured

    def set_configured(self, configured):
        self._configured = configured
        self.configured_changed.emit()

    @property
    def Afs(self):
        return self._afs

    def set_afs(self, afs):
        self._afs = afs
        self.afs_changed.emit()

    @property
    def Cvmfs(self):
        return self._cfmfs

    def set_cvmfs(self, cvmfs):
        self._cvmfs = cvmfs
        self.cvmfs_changed.emit()

    @property
    def Run(self):
        return self._run

    def set_run(self, run):
        self._run = run
        self.run_changed.emit()

    @property
    def AutoUpdate(self):
        return self._autoupdate

    def set_autoupdate(self, autoupdate):
        self._autoupdate = autoupdate
        self.autoupdate_changed.emit()

    @property
    def HomeDirectory(self):
        return self._homedirectory

    def set_homedirectory(self, homedirectory):
        self._homedirectory = homedirectory
        self.homedirectory_changed.emit()

    @property
    def LocmapReconfigure(self):
        return self._locmapreconfigure

    def set_locmapreconfigure(self, locmapreconfigure):
        self._locmapreconfigure = locmapreconfigure
        self.locmapreconfigure_changed.emit()

    property
    def GnomeInit(self):
        return self._gnomeinit

    def set_gnomeinit(self, gnomeinit):
        self._gnomeinit = gnomeinit
        self.gnomeinit_changed.emit()

    def install_with_tasks(self):
        """Return installation tasks.
        :return: a list of tasks
        """
        return [
            CernCustomizationsInstallationTask(run = self._run, afs = self._afs, homedirectory = self._homedirectory, autoupdate = self._autoupdate, cvmfs = self._cvmfs, gnomeinit = self._gnomeinit, locmapreconfigure = self._locmapreconfigure)
        ]
