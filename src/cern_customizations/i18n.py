__all__ = ["_", "N_"]

import sys
import gettext

_ = lambda x: gettext.translation("cern-anaconda-addon", fallback=True).gettext(x) if x != "" else ""
N_ = lambda x: x
