"""Module with the CernSpoke class."""
import logging

from cern_customizations.constants import CERN
from pyanaconda.ui.gui import GUIObject
from pyanaconda.ui.gui.spokes import NormalSpoke
from pyanaconda.ui.categories import SpokeCategory
from pyanaconda.ui.common import FirstbootOnlySpokeMixIn
from pyanaconda.ui.common import FirstbootSpokeMixIn

from cern_customizations.i18n import _, N_

class CernCategory(SpokeCategory):

    @staticmethod
    def get_title():
        return _("CERN")

    @staticmethod
    def get_sort_order():
        return 100

__all__ = ["CernSpoke"]

class CernSpoke(FirstbootOnlySpokeMixIn, NormalSpoke):
    """
    It will appear in the Initial Setup only

    :see: pyanaconda.ui.common.UIObject
    :see: pyanaconda.ui.common.Spoke
    :see: pyanaconda.ui.gui.GUIObject
    :see: pyanaconda.ui.common.FirstbootSpokeMixIn
    :see: pyanaconda.ui.gui.spokes.NormalSpoke
    """
    builderObjects = ["CernSpokeWindow", "mainbuttonImage"]
    mainWidgetName = "CernSpokeWindow"
    uiFile = "cern_firstboot.glade"
    category = CernCategory
    icon = "applications-science-symbolic"
    title = N_("_CERN CUSTOMIZATIONS")

    def __init__(self, data, storage, payload):
        NormalSpoke.__init__(self, data, storage, payload)
        self._cern_customizations_module = CERN.get_proxy()

    def initialize(self):
        NormalSpoke.initialize(self)
        self._cern = self.builder.get_object("radio_cern")
        self._not_cern = self.builder.get_object("radio_not_cern")
        self._afs = self.builder.get_object("checkbox_afs")
        #self._eos = self.builder.get_object("checkbox_eos")
        self._cvmfs = self.builder.get_object("checkbox_cvmfs")
        self._cern_autoupdate = self.builder.get_object("radio_cern_autoupdate_install")
        self._not_cern_autoupdate = self.builder.get_object("radio_not_cern_autoupdate_install")
        self._cern_homedirectory = self.builder.get_object("radio_cern_homedirectory")
        self._cern_homedirectory_afs = self.builder.get_object("radio_cern_homedirectory_afs")
        self._locmapreconfigure = self.builder.get_object("checkbox_locmapreconfigure")

    def refresh(self):
        pass

    def apply(self):
        self._cern_customizations_module.SetConfigured(True)

    def execute(self):
        pass

    @property
    def ready(self):
        return True

    @property
    def completed(self):
        pass

    @property
    def mandatory(self):
        configured = self._cern_customizations_module.Configured
        if configured:
            return False
        else:
            return True
    @property
    def status(self):
        configured = self._cern_customizations_module.Configured

        if configured:
            return _("Addon is configured\n\n ** Note **\n\n When clicking:\n'FINISH CONFIGURATION'\nPlease allow time for the customizations to be configured")
        else:
            return _("Addon is not configured")

    ### handlers ###
    def on_check_box_toggled(self, *args):
        if self._cern.get_active():
            self._cern_customizations_module.SetAfs(self._afs.get_active())
            #self._cern_customozations_module.eos = self._eos.get_active()
            self._cern_customizations_module.SetCvmfs(self._cvmfs.get_active())
            self._cern_customizations_module.SetLocmapReconfigure(self._locmapreconfigure.get_active())
        else:
            self._cern_customizations_module.SetAfs(False)
            self._cern_customizations_module.SetCvmfs(False)
            self._cern_customizations_module.SetLocmapReconfigure(False)
            #self._cern_customizations_module.eos = False

    def on_radio_button_toggled(self, *args):

        if self._cern.get_active():
            self._cern_customizations_module.SetRun(True)
            self._cern_customizations_module.SetAutoUpdate(self._cern_autoupdate.get_active())
            self._cern_customizations_module.SetHomeDirectory(self._cern_homedirectory.get_active())
            self._cern_customizations_module.SetAfs(self._afs.get_active())
            #self._cern_customizations_module.eos = self._eos.get_active()
            self._cern_customizations_module.SetCvmfs(self._cvmfs.get_active())

        if self._not_cern.get_active():
            self._cern_customizations_module.SetRun(False)
            self._cern_customizations_module.SetAutoUpdate(False)
            self._cern_customizations_module.SetAfs(False)

            #self._cern_customizations_module.eos = False
            self._cern_customizations_module.SetCvmfs(False)

    def on_entry_icon_clicked(self, entry, *args):
        """Handler for the textEntry's "icon-release" signal."""

        entry.set_text("")

    def on_main_button_clicked(self, *args):
        """Handler for the mainButton's "clicked" signal."""

        # every GUIObject gets ksdata in __init__
        dialog = CernFirstbootDialog(self.data)

        # show dialog above the lightbox
        with self.main_window.enlightbox(dialog.window):
            dialog.run()

class CernFirstbootDialog(GUIObject):
    """
    Not used but keeping code sample just in case we want to add
    some dialogs in the future
    """
    builderObjects = ["mainDialog"]
    mainWidgetName = "mainDialog"
    uiFile = "cern_firstboot.glade"

    def __init__(self, *args):
        GUIObject.__init__(self, *args)

    def initialize(self):
        GUIObject.initialize(self)

    def run(self):
        """
        Run dialog and destroy its window.

        :returns: respond id
        :rtype: int

        """

        ret = self.window.run()
        self.window.destroy()

        return ret
