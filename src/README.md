## Introduction ##

CERN anaconda add-on to replace old firstboot script.

## Developement ##

```
$ git pull
$ <modify code>
$ <edit spec file to reflect V-R release>
$ git add -p
$ git commit -m New version
$ git push
$ git tag vV-R
$ git push --tags
```

### Build ###
```
$ make {scratch,build}
```

