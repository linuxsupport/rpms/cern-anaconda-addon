### cern-anaconda-addon

This package provides the CERN addon for custom graphical 'wizard' (`initial-setup-graphical`) during first boot.

locmap will do the heavy lifting based on what users select from `initial-setup-graphical`

For development please check the `utils` directory.
